# Überauth Stripe
[![Build Status][travis-img]][travis] [![Hex Version][hex-img]][hex] [![License][license-img]][license]

[travis-img]: https://travis-ci.org/ueberauth/ueberauth_stripe.png?branch=master
[travis]: https://travis-ci.org/ueberauth/ueberauth_stripe
[hex-img]: https://img.shields.io/hexpm/v/ueberauth_stripe.svg
[hex]: https://hex.pm/packages/ueberauth_stripe
[license-img]: http://img.shields.io/badge/license-MIT-brightgreen.svg
[license]: http://opensource.org/licenses/MIT

> Stripe OAuth2 strategy for Überauth.

## Installation

1. Setup your application at [Stripe Connect](https://dashboard.stripe.com/account/applications/settings).

1. Add `:ueberauth_stripe` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:ueberauth_stripe, "~> 0.1"}]
    end
    ```

1. Add the strategy to your applications:

    ```elixir
    def application do
      [applications: [:ueberauth_stripe]]
    end
    ```

1. Add Stripe to your Überauth configuration:

    ```elixir
    config :ueberauth, Ueberauth,
      providers: [
        stripe: {Ueberauth.Strategy.Stripe, []}
      ]
    ```

1.  Update your provider configuration:

    ```elixir
    config :ueberauth, Ueberauth.Strategy.Stripe.OAuth,
      client_id: System.get_env("STRIPE_CLIENT_ID")
      client_secret: System.get_env("STRIPE_CLIENT_SECRET")
    ```

1.  Include the Überauth plug in your controller:

    ```elixir
    defmodule MyApp.AuthController do
      use MyApp.Web, :controller
      plug Ueberauth
      ...
    end
    ```

1.  Create the request and callback routes if you haven't already:

    ```elixir
    scope "/auth", MyApp do
      pipe_through :browser

      get "/:provider", AuthController, :request
      get "/:provider/callback", AuthController, :callback
    end
    ```

1. Your controller needs to implement callbacks to deal with `Ueberauth.Auth` and `Ueberauth.Failure` responses.

For an example implementation see the [Überauth Example](https://github.com/ueberauth/ueberauth_example) application.

## Calling

Depending on the configured URL you can initialize the request through:

    /auth/stripe

Or with options:

    /auth/stripe?scope=read_write

By default the requested scope is "read_only", if you want to make charges on behalf of the connected app you need to request "read_write". Scope can be configured either explicitly as a `scope` query value on the request path or in your configuration:

```elixir
config :ueberauth, Ueberauth,
  providers: [
    stripe: {Ueberauth.Strategy.Stripe, [default_scope: "read_write"]}
  ]
```

Additionally you can now specify the `display` param to pass to Stripe:

```elixir
config :ueberauth, Ueberauth,
  providers: [
    stripe: {Ueberauth.Strategy.Stripe, [
      default_scope: "read_write",
      display: "popup"
    ]}
  ]
```

`display` can be the following values: `page` (default), `async`, `iframe`, `popup`, `touch`, `wap`

## License

Please see [LICENSE](https://github.com/crystalhelix/ueberauth_stripe/blob/master/LICENSE) for licensing details.

Developed by [Crystal Helix Inc](https://www.crystalhelix.com/)