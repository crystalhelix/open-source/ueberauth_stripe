defmodule Ueberauth.Stripe.Mixfile do
  @moduledoc """
  Ueberauth Stripe package setup
  """
  use Mix.Project

  @version "0.1.0"
  @url "https://github.com/crystalhelix/ueberauth_stripe"

  def project do
    [app: :ueberauth_stripe,
     version: @version,
     name: "Ueberauth Stripe Strategy",
     package: package(),
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     source_url: @url,
     homepage_url: @url,
     description: description(),
     deps: deps(),
     docs: docs()]
  end

  def application do
    [applications: [:logger, :oauth2, :ueberauth]]
  end

  defp deps do
    [
      {:ueberauth, "~> 0.4"},
      {:oauth2, "~> 0.8"},
      {:credo, "~> 0.8", only: [:dev, :test]},
      {:ex_doc, "~> 0.16", only: :dev},
      {:earmark, ">= 0.0.0", only: :dev}
    ]
  end

  defp docs do
    [extras: ["README.md", "CONTRIBUTING.md"]]
  end

  defp description do
    "An Uberauth strategy for Stripe authentication."
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["Alejandro Carlos"],
      licenses: ["MIT"],
      links: %{"GitHub": @url},
      organization: "Crystal Helix Inc"
    ]
  end
end
