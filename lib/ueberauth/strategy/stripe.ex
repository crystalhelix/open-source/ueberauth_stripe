defmodule Ueberauth.Strategy.Stripe do
  @moduledoc """
  Stripe Strategy for Überauth.
  """

  use Ueberauth.Strategy, default_scope: "read_write",
                          default_stripe_landing: "login",
                          default_always_prompt: false,
                          default_response_type: "code",
                          allowed_request_params: [
                            :response_type,
                            :scope,
                            :state,
                            :stripe_landing,
                            :always_prompt
                          ]

  alias Ueberauth.Auth.Info
  alias Ueberauth.Auth.Credentials
  alias Ueberauth.Auth.Extra
  alias Ueberauth.Strategy.Stripe.OAuth

  @doc """
  Handles initial request for Stripe authentication.
  """
  def handle_request!(conn) do
    allowed_params = conn
     |> option(:allowed_request_params)
     |> Enum.map(&to_string/1)

    authorize_url = conn.params
      |> maybe_replace_param(conn, "response_type", :default_response_type)
      |> maybe_replace_param(conn, "scope", :default_scope)
      |> maybe_replace_param(conn, "state", :state)
      |> maybe_replace_param(conn, "stripe_landing", :default_stripe_landing)
      |> maybe_replace_param(conn, "always_prompt", :default_always_prompt)
      |> Enum.filter(fn {k, _v} -> Enum.member?(allowed_params, k) end)
      |> Enum.map(fn {k, v} -> {String.to_existing_atom(k), v} end)
      |> Keyword.put(:redirect_uri, callback_url(conn))
      |> OAuth.authorize_url!

    redirect!(conn, authorize_url)
  end

  @doc """
  Handles the callback from Facebook.
  """
  def handle_callback!(%Plug.Conn{params: %{"code" => code}} = conn) do
    opts = [redirect_uri: callback_url(conn)]
    client = OAuth.get_token!([code: code], opts)
    token = client.token

    if token.access_token == nil do
      err = token.other_params["error"]
      desc = token.other_params["error_description"]
      set_errors!(conn, [error(err, desc)])
    else
      set_oauth_results(conn, client)
    end
  end

  @doc false
  def handle_callback!(conn) do
    set_errors!(conn, [error("missing_code", "No code received")])
  end

  @doc false
  def handle_cleanup!(conn) do
    conn
    |> put_private(:stripe_user, nil)
    |> put_private(:stripe_token, nil)
  end

  @doc """
  Fetches the uid field from the response.
  """
  def uid(conn) do
    conn.private.stripe_user.other_params["stripe_user_id"]
  end

  @doc """
  Includes the credentials from the facebook response.
  """
  def credentials(conn) do
    token = conn.private.stripe_token
    scopes = token.other_params["scope"] || ""
    scopes = String.split(scopes, ",")

    _ = """
    Stripe returns the following data after the code request:

    "access_token": "{ACCESS_TOKEN}",
    "scope": "express"
    "livemode": false,
    "refresh_token": "{REFRESH_TOKEN}",
    "token_type": "bearer",
    "stripe_publishable_key": "{PUBLISHABLE_KEY}",
    "stripe_user_id": "{ACCOUNT_ID}",
    """

    %Credentials{
      token: token.access_token,
      refresh_token: token.refresh_token,
      token_type: token.token_type,
      scopes: scopes,
      other: %{
        live_mode: token.other_params["livemode"],
        stripe_user_id: token.other_params["stripe_user_id"],
        stripe_publishable_key: token.other_params["stripe_publishable_key"]
      }
    }
  end

  @doc """
  Fetches the fields to populate the info section of the
  `Ueberauth.Auth` struct.
  """
  def info(_conn) do
    %Info{}
  end

  @doc """
  Stores the raw information (including the token) obtained from
  the facebook callback.
  """
  def extra(conn) do
    %Extra{
      raw_info: %{
        token: conn.private.stripe_token
      }
    }
  end

  defp set_oauth_results(conn, client) do
    conn
    |> put_private(:stripe_token, client.token)
    |> put_private(:stripe_user, client.token)
  end

  defp option(conn, key) do
    default = Keyword.get(default_options(), key)

    conn
    |> options
    |> Keyword.get(key, default)
  end
  defp option(nil, conn, key), do: option(conn, key)
  defp option(value, _conn, _key), do: value

  defp maybe_replace_param(params, conn, name, config_key) do
    if params[name] do
      params
    else
      Map.put(
        params,
        name,
        option(params[name], conn, config_key)
      )
    end
  end
end
